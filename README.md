# dan

A blogging tool that uses IMAP4 as storage

# About

This program iterates all email messages in the INBOX of an IMAP4 account, treats them as posts or comments of a blog and generates a set of static HTML files. It's written in Python3 with no external dependencies.

This is not the user manual; man pages `dan(1)` and `dan(5)` are.

# Installation

Run `make install` as root.

It's a simple script, so you don't need to install it if you don't want to. Just run it. It's up to you.

# Example usage

This command line reads all emails from the `imap4.example.com` account using the specified credentials and generates static blog content into the `/var/www/htdocs/blog` directory. The `lieutenant@example.com` and `forrest@othersite.com` email addresses are allowed to post blog entries by writing emails to `blog@example.com`. Each generated HTML file contains a link to post a comment to it.

```
dan server=imap4.example.com user=usrthere passwd=seekr1t \
    eaddr=blog@example.com \
    allow=lieutenant@example.com allow=forrest@othersite.com \
    cssurl=style.css cssurl=https://example.com/styles/main.css \ 
    baseurl=https://example.com/blog/ \
    "title=Lieutenant Dan's Blog" \
    outdir=/var/www/htdocs/blog/
```

You can also write all these arguments in a configuration file (one in each line) and use the `cfgfile` option.

# License and author

See the LICENSE file for details.
