PREFIX=/usr/local
PREFIX_MAN=$(PREFIX)/man

all:
	@echo "Run 'make install' to install."

install:
	install dan $(PREFIX)/bin
	mkdir -p -m 755 $(PREFIX_MAN)/man1
	install -m 644 dan.1 $(PREFIX_MAN)/man1/dan.1
	mkdir -p -m 755 $(PREFIX_MAN)/man5
	install -m 644 dan.5 $(PREFIX_MAN)/man5/dan.5
